package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class P {

	
		@Test
		public void testIsPalindrome() {
			assertTrue("Invalid value for palindrome", Palindrome.isPalindrome( "anana" ));
		}
		
		@Test
		public void testIsPalindromeNegative() {
			assertFalse("Invalid value for palindrome", Palindrome.isPalindrome( "Anna has a lamb" ));
		}
		
		@Test
		public void testIsPalindromeBoundaryIn () {
			assertFalse("Invalid value for palindrome", Palindrome.isPalindrome("Aa"));
		}

		@Test
		public void testIsPalindromeBoundaryOut () {
			assertFalse("Invalid value for palindrome", Palindrome.isPalindrome("racer car"));
		}
	

}
